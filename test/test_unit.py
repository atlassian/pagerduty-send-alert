import io
import os
import sys
from copy import copy
from contextlib import contextmanager
from http import HTTPStatus
from unittest import TestCase

import pytest

from pipe.pipe import PagerDutySendAlertPipe, schema, PAGER_DUTY_SEND_ALERT_URL


@contextmanager
def capture_output():
    standard_out = sys.stdout
    try:
        stdout = io.StringIO()
        sys.stdout = stdout
        yield stdout
    finally:
        sys.stdout = standard_out
        sys.stdout.flush()


class CreateIncidentTestCase(TestCase):
    @pytest.fixture(autouse=True)
    def inject_fixtures(self, caplog, mocker, requests_mock):
        self.caplog = caplog
        self.mocker = mocker
        self.request_mock = requests_mock

    def setUp(self):
        self.sys_path = copy(sys.path)
        sys.path.insert(0, os.getcwd())

    def tearDown(self):
        sys.path = self.sys_path

    def test_default_success(self):
        self.mocker.patch.dict(
            os.environ, {
                "API_KEY": "test-api-key",
                "INTEGRATION_KEY": "test-integration-key"
            }
        )

        pipe = PagerDutySendAlertPipe(
            schema=schema, check_for_newer_version=True)

        self.request_mock.register_uri(
            'POST', PAGER_DUTY_SEND_ALERT_URL,
            text='',
            status_code=HTTPStatus.ACCEPTED
        )

        with capture_output() as out:
            pipe.run()

        self.assertRegex(out.getvalue(), 'Alert successfully sent.')

    def test_fail(self):
        self.mocker.patch.dict(
            os.environ, {
                "API_KEY": "test-api-key",
                "INTEGRATION_KEY": "test-integration-key"
            }
        )

        pipe = PagerDutySendAlertPipe(
            schema=schema, check_for_newer_version=True)

        self.request_mock.register_uri(
            'POST', PAGER_DUTY_SEND_ALERT_URL,
            text='',
            status_code=HTTPStatus.NOT_FOUND
        )

        with capture_output() as out:
            with self.assertRaises(SystemExit) as exc_context:
                pipe.run()
            self.assertEqual(exc_context.exception.code, 1)

        self.assertRegex(
            out.getvalue(),
            '✖ Failed to send request to Pagerduty.'
        )

    def test_no_api_key(self):
        self.mocker.patch.dict(
            os.environ, {
                "INTEGRATION_KEY": "test-integration-key"
            }
        )

        with capture_output() as out:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                PagerDutySendAlertPipe(
                    schema=schema, check_for_newer_version=True)

        self.assertIn('API_KEY:\n- required field', out.getvalue())
        self.assertEqual(pytest_wrapped_e.type, SystemExit)
