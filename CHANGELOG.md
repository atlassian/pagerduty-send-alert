# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.7.1

- patch: Internal maintenance: Bump release pipe version. Add multi-platform build: linux/arm64 and linux/amd64.

## 0.7.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.6.0

- minor: Bump bitbucket-pipes-toolkit to fix vulnerabilities.

## 0.5.0

- minor: Update bitbucket-pipes-toolkit to fix vulnerabilities with certify and gitpython.
- patch: Internal maintenance: bump pipes versions in pipelines config file.

## 0.4.2

- patch: Internal maintenance: refactor tests to unit tests.
- patch: Internal maintenance: update community link.
- patch: Internal maintenance: update packages and release process.

## 0.4.1

- patch: Internal maintenance: add bitbucket-pipe-release.

## 0.4.0

- minor: Internal maintenance: bump bitbucket-pipes-toolkit version.

## 0.3.7

- patch: Internal maintenance: change pipe metadata according to new structure

## 0.3.6

- patch: Internal maintenance: Add gitignore secrets.

## 0.3.5

- patch: Update the Readme with a new Atlassian Community link.

## 0.3.4

- patch: Internal maintenance: Add hadolint linter for Dockerfile

## 0.3.3

- patch: Added code style checks

## 0.3.2

- patch: Documentation updates

## 0.3.1

- patch: Internal maintenance: update pipes toolkit version

## 0.3.0

- minor: Change default environment variable BITBUCKET_REPO_OWNER to BITBUCKET_WORKSPACE due to deprecation in Bitbucket API.

## 0.2.2

- patch: Minor documentation updates

## 0.2.1

- patch: Updated contributing guidelines

## 0.2.0

- minor: Fix passing of client_url and source inside incident details.
- minor: Removed unused pipe variables from the README.md

## 0.1.2

- patch: Fixed colourised logging.

## 0.1.1

- patch: Fixed missing variables

## 0.1.0

- minor: Initial release
- minor: Initial release of the pipe
