# Bitbucket Pipelines Pipe: PagerDuty Send Alert

Sends alert to [PagerDuty][pagerduty].

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/pagerduty-send-alert:0.7.1
  variables:
    API_KEY: $API_KEY
    INTEGRATION_KEY: $INTEGRATION_KEY
    # INCIDENT_KEY: '<string>' # Optional.
    # EVENT_TYPE: '<string>' # Optional.
    # SEVERITY: '<string>' # Optional.
    # DESCRIPTION: '<string>' # Optional.
    # CLIENT: '<string>' # Optional.
    # CLIENT_URL: '<string>' # Optional.
    # DEBUG: '<boolean>' # Optional.
```
## Variables

| Variable            | Usage                                                                                  |
|---------------------|----------------------------------------------------------------------------------------|
| API_KEY (*)         | PagerDuty API key                                                                      |
| INTEGRATION_KEY (*) | Service integration key                                                                |
| INCIDENT_KEY        | PageDuty incident key. Default: `${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}-event` |
| EVENT_TYPE          | Type of the event. Default: `trigger`                                                  |
| SEVERITY            | Severity of the event. Default: `error`                                                |
| DESCRIPTION         | Short description of the event. Default: `Event triggered from Bitbucket Pipelines`    |
| CLIENT              | The name of the client application. Default: `Bitbucket Pipelines`                     |
| CLIENT_URL          | URL of the sending entity, default to the pipeline URL                                 |
| DEBUG               | Turn on extra debug information. Default: `false`.                                     |

_(*) = required variable._

## Prerequisites

To send alerts to PagerDuty, you need an API key. You can follow the instructions [here][pagerduty api key] to create one.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/pagerduty-send-alert:0.7.1
    variables:
      API_KEY: $API_KEY
      INTEGRATION_KEY: $INTEGRATION_KEY
```

Advanced example:

```yaml
script:
  - pipe: atlassian/pagerduty-send-alert:0.7.1
    variables:
      API_KEY: $API_KEY
      INTEGRATION_KEY: $INTEGRATION_KEY
      INCIDENT_KEY: pagerduty-incident
      SEVERITY: 'critical'
      DETAILS: 'Server is on fire!'

```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce

## License
Copyright (c) 2018 Atlassian and others.
Apache 2.0 licensed, see [LICENSE.txt](LICENSE.txt) file.

[pagerduty]: https://pagerduty.com
[pagerduty api key]: https://support.pagerduty.com/docs/using-the-api#section-generating-a-general-access-rest-api-key
[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-questions?add-tags=bitbucket-pipelines,pipes,notify,pagerduty,alert
