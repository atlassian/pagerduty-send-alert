import os
from http import HTTPStatus

import yaml
import requests

from bitbucket_pipes_toolkit import Pipe


PAGER_DUTY_SEND_ALERT_URL = 'https://events.pagerduty.com/v2/enqueue'
DEFAULT_INCIDENT_KEY = f"{os.environ['BITBUCKET_WORKSPACE']}/{os.environ['BITBUCKET_REPO_SLUG']}-event"
DEFAULT_PIPELINES_URL = f"https://bitbucket.org/{os.environ['BITBUCKET_WORKSPACE']}/{os.environ['BITBUCKET_REPO_SLUG']}/pipelines/results/{os.environ['BITBUCKET_BUILD_NUMBER']}"


schema = {
    "API_KEY": {
        "type": "string",
        "required": True
    },
    "INTEGRATION_KEY": {
        "type": "string",
        "required": True
    },
    "INCIDENT_KEY": {
        "type": "string",
        "default": DEFAULT_INCIDENT_KEY
    },
    "EVENT_TYPE": {
        "type": "string",
        "default": 'trigger'
    },
    "SEVERITY": {
        "type": "string",
        "default": 'error'
    },
    "DESCRIPTION": {
        "type": "string",
        "default": 'Event triggered from Bitbucket Pipelines'
    },
    "CLIENT": {
        "type": "string",
        "default": 'Bitbucket Pipelines'
    },
    "CLIENT_URL": {
        "type": "string",
        "default": DEFAULT_PIPELINES_URL
    },
    "DEBUG": {
        "type": "boolean",
        "default": False
    },
}


class PagerDutySendAlertPipe(Pipe):

    def __init__(self, pipe_metadata=None, schema=None, env=None, check_for_newer_version=False):
        super().__init__(
            pipe_metadata=pipe_metadata,
            schema=schema,
            env=env,
            check_for_newer_version=check_for_newer_version
        )
        self.api_key = self.get_variable('API_KEY')
        self.integration_key = self.get_variable('INTEGRATION_KEY')
        self.incident_key = self.get_variable('INCIDENT_KEY')
        self.event_type = self.get_variable('EVENT_TYPE')
        self.severity = self.get_variable('SEVERITY')
        self.description = self.get_variable('DESCRIPTION')
        self.client = self.get_variable('CLIENT')
        self.client_url = self.get_variable('client_url')

    def run(self):
        super().run()

        header = {
            'Content-Type': 'application/json',
            'Authorization': f'Token token={self.api_key}'
        }

        payload = {
            "routing_key": self.integration_key,
            "event_action": self.event_type,
            "dedup_key": self.incident_key,
            "client": self.client,
            "client_url": self.client_url,
            "payload": {
                "summary": self.description,
                "source": DEFAULT_PIPELINES_URL,
                "severity": self.severity
            }
        }

        self.log_info('Sending an alert to PageDuty...')

        response = requests.post(
            PAGER_DUTY_SEND_ALERT_URL,
            json=payload,
            headers=header)

        if response.status_code != HTTPStatus.ACCEPTED:
            self.fail(
                f'Failed to send request to Pagerduty. Expected HTTP Status 202, got {response.status_code}.'
                f' Error message: {response.text}'
            )

        self.success('Alert successfully sent.')


if __name__ == '__main__':
    with open('/pipe.yml') as f:
        metadata = yaml.safe_load(f.read())
    pipe = PagerDutySendAlertPipe(pipe_metadata=metadata, schema=schema, check_for_newer_version=True)
    pipe.run()
